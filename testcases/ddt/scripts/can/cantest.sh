#! /bin/bash
###############################################################################
# Copyright (C) 2011 Texas Instruments Incorporated -
# http://www.ti.com/ # # This program is free software; you can
# redistribute it and/or # modify it under the terms of the GNU General
# Public License as # published by the Free Software Foundation version 2
#
# This program is distributed "as is" WITHOUT ANY WARRANTY of any
# kind, whether express or implied; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###############################################################################
. "common.sh"  # Import do_cmd(), die() and other functions

DEFAULT_BITRATE='1000000'
DEFAULT_CAN_IFACE='mcu_mcan0'
TEST_ALL_INTERFACES=false

INIT_STAT_RX=0;
INIT_STAT_TX=0;
PREFINAL_STAT_RX=0;
PREFINAL_STAT_TX=0;
FINAL_STAT_RX=0;
FINAL_STAT_TX=0;
INIT_ERRSTAT_RX=0;
INIT_ERRSTAT_TX=0;
FINAL_ERRSTAT_RX=0;
FINAL_ERRSTAT_TX=0;

############################# Functions #######################################
usage()
{
	echo "cantest.sh <interface - mcu_mcan0> <bitrate> <dbitrate> <test to run - loopback or modular> "
	exit 1
}

set_can_interface()
{
	can_iface="$1"
	status="$2"
	do_cmd "ip link set $can_iface down";
	do_cmd "ip link set $can_iface $status";
}

send_packets()
{
	can_iface="$1"
	command="$2"
	mode="$3"
	if [[ $command == 'start' ]]; then
		do_cmd "candump -d -s 2 $can_iface &";
		if [[ $mode == 'fd' ]]; then do_cmd "cangen -b -L 16 $can_iface &";
		else do_cmd "cangen -L 16 $can_iface &"; fi
	else
		do_cmd "killall candump";
		do_cmd "killall cangen";
	fi
}

wait_for_stats()
{
	can_iface="$1"
	stat="/proc/net/can/stats"
	loop="0"

	while [ ! -e "$stat" ] && [ "$loop" -le "5" ]; do do_cmd "sleep 1"; echo "Waiting for $stat" ; loop=$((loop+1)); done;
	if [ ! -e "$stat" ]; then set_can_interface "$can_iface" 'down'; die "Failed to find stats in $stat"; fi;

}

get_stats()
{
	can_iface="$1"
	stage="$2"
	type="$3"
	if [[ $type == 'error' ]]; then
		tx_err=$(get_can_error_stats.sh -i "$can_iface" -s 'tx');
		rx_err=$(get_can_error_stats.sh -i "$can_iface" -s 'rx');
		if [ "$stage" == 'init' ]; then
			INIT_ERRSTAT_TX=$tx_err;
			INIT_ERRSTAT_RX=$rx_err;
		else
			FINAL_ERRSTAT_TX=$tx_err;
			FINAL_ERRSTAT_RX=$rx_err;
		fi
	else
		wait_for_stats "$can_iface";
		txf=$(get_can_stats.sh -s 'TXF');
		rxf=$(get_can_stats.sh -s 'RXF');
		if [ "$stage" == 'init' ]; then
			INIT_STAT_TX=$txf;
			INIT_STAT_RX=$rxf;
		elif [ "$stage" == 'prefinal' ]; then
			PREFINAL_STAT_TX=$txf;
			PREFINAL_STAT_RX=$rxf;
		else
			FINAL_STAT_TX=$txf;
			FINAL_STAT_RX=$rxf;
		fi
	fi
}

compare_stats()
{
	stats=$1
	case $stats in
		error)
			echo "Dump error stats before compare: [$FINAL_ERRSTAT_TX,$INIT_ERRSTAT_TX,$FINAL_ERRSTAT_RX,$INIT_ERRSTAT_RX]"
			if [ "$FINAL_ERRSTAT_TX" == "$INIT_ERRSTAT_TX" ] && \
			[ "$FINAL_ERRSTAT_RX" == "$INIT_ERRSTAT_RX" ]; then
				echo "TX err stats | Final: $FINAL_ERRSTAT_TX == init: $INIT_ERRSTAT_TX";
				echo "RX err stats | Final: $FINAL_ERRSTAT_RX == init: $INIT_ERRSTAT_RX";
			else exit 1; fi;
		;;
		three_stage)
			echo "Dump stats before compare: [$FINAL_STAT_TX,$PREFINAL_STAT_TX,$INIT_STAT_TX,$FINAL_STAT_RX,$PREFINAL_STAT_RX,$INIT_STAT_RX]"
			if [ "$FINAL_STAT_TX" -gt "$PREFINAL_STAT_TX" ] && \
			[ "$FINAL_STAT_RX" -gt "$PREFINAL_STAT_RX" ] && \
			[ "$PREFINAL_STAT_TX" -gt "$INIT_STAT_TX" ] && \
			[ "$PREFINAL_STAT_RX" -gt "$INIT_STAT_RX" ]; then
				echo "TX stats | Final: $FINAL_STAT_TX > Prefinal: $PREFINAL_STAT_TX";
				echo "RX stats | Final: $FINAL_STAT_RX > Prefinal: $PREFINAL_STAT_RX";
				echo "TX stats | Prefinal: $PREFINAL_STAT_TX > init: $INIT_STAT_TX";
				echo "RX stats | Prefinal: $PREFINAL_STAT_RX > init: $INIT_STAT_RX";
			else exit 1; fi;
		;;
		two_stage)
			echo "Dump stats before compare: [$FINAL_STAT_TX,$INIT_STAT_TX,$FINAL_STAT_RX,$INIT_STAT_RX]"
			if [ "$FINAL_STAT_TX" -gt "$INIT_STAT_TX" ] && \
			[ "$FINAL_STAT_RX" -gt "$INIT_STAT_RX" ]; then
				echo "TX stats | Final: $FINAL_STAT_TX > init: $INIT_STAT_TX";
				echo "RX stats | Final: $FINAL_STAT_RX > init: $INIT_STAT_RX";
			else exit 1; fi;
			;;
		*)
		end 1;;
	esac
}

suspend()
{
	can_iface="$1"
	bitrate="$2"
	do_cmd config_can_interface.sh -i "$can_iface" -c 'ip_link' -b "$bitrate";
	set_can_interface "$can_iface" 'up';
	init_state=$(cat /sys/class/net/"$can_iface"/operstate);
	do_cmd "rtcwake -s 5 -m mem";
	final_state=$(cat /sys/class/net/"$can_iface"/operstate);
	set_can_interface "$can_iface" 'down';
	if [ "$init_state" != "$final_state" ]; then die "Suspend resume did not restore CAN state"; fi;
}

modular_suspend()
{
	can_iface="$1"
	bitrate="$2"
	do_cmd config_can_interface.sh -i "$can_iface" -c 'ip_link' -b "$bitrate" -l;
	set_can_interface "$can_iface" 'up';
	send_packets "$can_iface" 'start';
	get_stats "$can_iface" 'init';
	do_cmd "rtcwake -s 5 -m mem";
	get_stats "$can_iface" 'prefinal';
	do_cmd "sleep 5";
	get_stats "$can_iface" 'final';
	send_packets "$can_iface" 'stop';
	set_can_interface "$can_iface" 'down';
	echo "==============================================================";
	echo "Dump transmitted and received frames from: /proc/net/can/stats";
	compare_stats 'three_stage';
	echo "==============================================================";
}

loopback_one()
{
	can_under_test="$1"
	bitrate="$2"
	dbitrate="$3"
	set_can_interface "$can_under_test" 'down';
	do_cmd config_can_interface.sh -i "$can_under_test"  -c 'ip_link' -b "$bitrate" -d "$dbitrate" -l -f;
	set_can_interface "$can_under_test" 'up';
	send_packets "$can_under_test" 'start' 'fd';
	get_stats "$can_under_test" 'init';
	get_stats "$can_under_test" 'init' 'error';
	do_cmd "sleep 5";
	get_stats "$can_under_test" 'final';
	get_stats "$can_under_test" 'final' 'error';
	send_packets "$can_under_test" 'stop';
	set_can_interface "$can_under_test" 'down';
	echo "==============================================================";
	echo "Dump transmitted and received frames from: /proc/net/can/stats";
	compare_stats 'two_stage';
	echo "Dump Error stats from: /sys/class/net/$can_under_test/statistics";
	compare_stats 'error';
	echo "==============================================================";
}

loopback_all()
{
	bitrate="$1"
	dbitrate="$2"
	echo "Getting CAN Interfaces for $MACHINE"
	cans=$(get_can_interfaces.sh $MACHINE)

	if [ -z "$cans" ]; then	die "No CAN Interface found for the platform $MACHINE";	fi;

	echo "Available CANs for $MACHINE : |$cans|"
	for can in $cans
	do
		loopback_one "$can" "$bitrate" "$dbitrate"
	done
}

loopback()
{
	can_iface="$1"
	bitrate="$2"
	dbitrate="$3"
	if [[ "$TEST_ALL_INTERFACES" == "true" ]]; then
		loopback_all "$bitrate" "$dbitrate"
	else
		loopback_one "$can_iface" "$bitrate" "$dbitrate"
	fi
}

modular_one()
{
	can_under_test=$1
	echo "Running CAN Modular Test on $can_under_test"
	can_interface="/sys/class/net/$can_under_test";
	if ! [ -d "$can_interface" ]; then die "Check dtb to see if $can_under_test is included"; fi;
	can_module=$(zcat /proc/config.gz |grep CONFIG_CAN=m);
	if [ -z "$can_module" ]; then die "Check Configs to see if CAN is included"; fi;
}

modular_all()
{
	echo "Getting CAN Interfaces for $MACHINE"
	cans=$(get_can_interfaces.sh $MACHINE)

	if [ -z "$cans" ]; then	die "No CAN Interface found for the platform $MACHINE";	fi;

	echo "Available CANs for $MACHINE : |$cans|"
	for can in $cans
	do
		modular_one "$can"
	done
}

modular()
{
	can_iface="$1"
	if [[ "$TEST_ALL_INTERFACES" == "true" ]]; then
		modular_all
	else
		modular_one "$can_iface"
	fi

}

################################ CLI Params ####################################

while [ $# -gt 0 ]
do
	case $1 in
	-m|--modular)
		test="modular" ;;
	-s|--suspend)
		test="suspend" ;;
	-c|--modular_suspend)
		test="modular_suspend" ;;
	-l|--loopback)
		test="loopback" ;;
	--all_interfaces)
		TEST_ALL_INTERFACES=true ;;
	-i|--interface)
		interface="$2" ; shift;;
	-b|--bitrate)
		bitrate="$2" ; shift;;
	-d|--dbitrate)
		dbitrate="$2" ; shift;;
	(--)
	  shift; break;;
	(-*)
	  echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
	(*)
	  break;;
	esac
	shift
done

interface=$(echo "$interface" | tr -d "\"\'\`");
bitrate=$(echo "$bitrate" | tr -d "\"\'\`");
dbitrate=$(echo "$dbitrate" | tr -d "\"\'\`");
iface="${iface:=$DEFAULT_CAN_IFACE}"
brate="${bitrate:=$DEFAULT_BITRATE}"
dbrate="${dbitrate:=$DEFAULT_BITRATE}"

if [ -n "$interface" ]; then iface=$interface; fi;

case $test in
  modular)
	modular "$iface"
	;;
  suspend)
	suspend "$iface" "$brate"
	;;
  modular_suspend)
	modular_suspend "$iface" "$brate"
	;;
  loopback)
	loopback "$iface" "$brate" "$dbrate"
	;;
  *)
	end 1
	;;
esac
